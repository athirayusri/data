import csv, sqlite3

with open('MOCK_DATA.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')

conn = sqlite3.connect('datawithoutpandas.db')
c = conn.cursor()
c.execute('CREATE TABLE IF NOT EXISTS data (id int, first_name text, last_name text, email text, gender text, ip_address text)')
    
with open ('MOCK_DATA.csv') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    to_db = [(i['id'], i['first_name'], i['last_name'], i['email'], i['gender'], i['ip_address']) for i in csv_reader]

c.executemany("INSERT INTO data VALUES (?, ?, ?, ?, ?, ?);", to_db)
conn.commit()
conn.close()