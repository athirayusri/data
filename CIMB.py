import requests
import json
import csv
import pandas as pd

request = requests.get('https://www.cimb.com.my/en/personal/help-support/branch-locator/_jcr_content/root/responsivegrid_197111812/branch_locator_copy.data')

data = json.loads(request.text)
cimbdata = data['location'][0]['areas'][0]['branches']
cimbdata = data['location'][0]['areas'][0]['branches']

with open('CIMB.json', 'w') as outfile:
    json.dump(cimbdata, outfile, indent=4,)

with open("CIMB.json") as j_file:
    cimbdata = json.load(j_file)

i = 0
temp = cimbdata

while i < 16:
    i += 1
    bankdata = data['location'][0]['areas'][i]['branches']
        
    with open('CIMB.json', 'w') as f:
        json.dump(bankdata, f, indent=4)        
        bankdata = json.load(f)

    with open("CIMB.json") as json_file:
        bankdata = json.load(json_file)
       
        temp.append(bankdata)

df = pd.read_json(r'./CIMB.json')
df.to_csv(r'./CIMB.csv', index = None, columns = ['branchName', 'branchAddress', 'latitude', 'longitude'])