import csv

with open ('MOCK_DATA.csv', 'r') as d1, open ('dbtocsv.csv', 'r') as d2:
    csvone = d1.readlines()
    csvtwo = d2.readlines()

with open ('differencecsv.csv', 'w') as d3:
    for line in csvtwo:
        if line not in csvone:
            d3.write(line)