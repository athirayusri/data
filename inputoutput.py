import csv, sqlite3, pandas as pd

with open('MOCK_DATA.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')

conn = sqlite3.connect('data.db')
c = conn.cursor()
c.execute('CREATE TABLE data (id int, first_name text, last_name text, email text, gender text, ip_address text)')
users = pd.read_csv('MOCK_DATA.csv')
users.to_sql('data', conn, if_exists = 'append', index = False)