import requests
import json
import csv
import pandas as pd

url = 'https://metizapps.com/storelocator/stores/storeDataGet'
headers = {'authority': 'metizapps.com', 'method': 'POST', 'path': '/storelocator/stores/getStoreFilter', 'scheme': 'https', 'accept': 'application/json, text/javascript, */*; q=0.01', 'accept-encoding': 'gzip, deflate, br', 'accept-language': 'en-US,en;q=0.9', 'content-length': '72', 'content-type': 'application/x-www-form-urlencoded; charset=UTF-8', 'origin': 'https://www.bonia.com', 'referer': 'https://www.bonia.com/', 'sec-fetch-dest': 'empty', 'sec-fetch-mode': 'cors', 'sec-fetch-site': 'cross-site', 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36'}
request = requests.get(url, headers = headers)

data = json.loads(request.text)

with open('Bonia.json', 'w') as outfile:
    json.dump(data, outfile, indent=4,)

with open("Bonia.json") as j_file:
    data = json.load(j_file)

df = pd.read_json(r'./Bonia.json')
#df.to_csv(r'./Bonia.csv', index = None, columns = ['name', 'streetaddress', 'loc_lat', 'loc_long'])