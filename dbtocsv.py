import sqlite3
import pandas as pd

c = sqlite3.connect('datawithoutpandas.db', isolation_level = None, detect_types = sqlite3.PARSE_COLNAMES)
table = pd.read_sql_query('SELECT * FROM data', c)
table.to_csv('dbtocsv.csv', index = False)
